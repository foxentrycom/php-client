<?php

namespace FoxentryPHP;

class request
{

    private ?string $endpoint;
    private ?array $query = array();
    private ?array $options = array();
    private string|int|null $customId = null;

    /**
     * @return void
     */
    private function reset(): void
    {
        $this->endpoint = null;
        $this->query    = array();
        $this->options  = array();
        $this->customId = null;
    }

    /**
     * @param string $endpoint
     * @return $this
     */
    function setEndpoint(string $endpoint): static
    {
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @param array $query
     * @return $this
     */
    function setQuery(array $query): static
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    function setOptions(array $options): static
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @param string $parameter
     * @param string|int|float|null $value
     * @return $this
     */
    protected function setQueryParameter(string $parameter, string|int|float|null $value): static
    {
        $this->query[$parameter] = $value;
        return $this;
    }

    /**
     * @param string $parameter
     * @param string|int|float|null $value
     * @return $this
     */
    protected function setQueryFilterParameter(string $parameter, string|int|float|null $value): static
    {
        if (!isset($this->query["filter"])) {
            $this->query["filter"] = array();
        }

        $this->query["filter"][$parameter] = $value;
        return $this;
    }


    /**
     * @param string $name
     * @param string|int|float|bool $value
     * @return $this
     */
    protected function setOption(string $name, string|int|float|bool $value): static
    {
        $this->options[$name] = $value;
        return $this;
    }

    /**
     * @param string|int $customId
     * @return $this
     */
    function setCustomId(string|int $customId): static
    {
        $this->customId = $customId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function build(): mixed
    {
        $data = array(
            "endpoint" => $this->endpoint,
            "query"    => $this->query,
            "options"  => $this->options,
            "customId" => $this->customId
        );

        $this->reset();

        return helper::decodeJSON(helper::encodeJSON($data));
    }

}