<?php

namespace FoxentryPHP;

use DI\Container;
use GuzzleHttp\Exception\GuzzleException;

class client
{

    private \GuzzleHttp\Client $httpClient;
    private string $apiKey;
    private array $client = array();
    private array $requests = array();
    private responses $responses;

    private string $apiUrl = "https://api.foxentry.com";
    //private string $apiUrl = "https://production.api.foxentry.com";
    private string $apiVersion;
    private string $apiLibVersion = "2.0";
    private string $batchEndpoint = "batch";
    private ?string $responseText;

    public function __construct()
    {
        $container = new Container();

        $this->httpClient = $container->get("\\GuzzleHttp\\Client");
        $this->responses  = $container->get("\\FoxentryPHP\\responses");

    }

    /**
     * @param $apiKey
     * @return $this
     */
    function setApiKey($apiKey): client
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @param string $apiUrl
     * @return $this
     */
    function setApiUrl(string $apiUrl): client
    {
        $this->apiUrl = $apiUrl;
        return $this;
    }

    /**
     * @param string $apiVersion
     * @return $this
     */
    function setApiVersion(string $apiVersion): static
    {
        $this->apiVersion = $apiVersion;
        return $this;
    }

    /**
     * @param string $country
     * @return $this
     */
    function setClientCountry(string $country): static
    {
        $this->client["country"] = $country;
        return $this;
    }

    /**
     * @param $latitude
     * @param $longitude
     * @return $this
     */
    function setClientLocation($latitude, $longitude): static
    {
        $this->client["location"] = array(
            "lat" => $latitude,
            "lon" => $longitude
        );
        return $this;
    }

    /**
     * @param request $request
     * @return $this
     */
    function addRequest(request $request): static
    {
        $this->requests[] = $request->build();
        return $this;
    }

    /**
     * @return array
     */
    private function buildApiCallData(): array
    {
        return array(
            "apiKey" => $this->apiKey,
            "apiVersion" => $this->apiVersion,
            "apiLibVersion" => $this->apiLibVersion,
            "requests" => $this->requests,
            "client" => $this->client
        );
    }

    /**
     * @return responses|mixed
     * @throws GuzzleException
     * @throws \Exception
     */
    function send(): mixed
    {
        try {
            $apiCall = $this->httpClient->post(
                $this->apiUrl."/".$this->getEndpoint(),
                [
                    "json" => $this->buildApiCallData()
                ]
            );

            $this->reset();

            $this->responseText = $apiCall->getBody()->getContents();

            if (!helper::isValidJSON($this->responseText)) {
                throw new \Exception("FOXENTRY API response not JSON");
            }

            $response = helper::decodeJSON($this->responseText);

            if ($this->isBatchResponse($response)) {
                return $this->responses->load($response);
            }
            else {
                return $this->responses->loadSingle($response);
            }
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
            return $this->handleErrorResponse($e);
        }
        catch (\GuzzleHttp\Exception\ServerException $e) {
            return $this->handleErrorResponse($e);
        }
        catch (\Exception $e) {
            throw new \Exception("FOXENTRY API ERROR: ".$e->getMessage());
        }
    }

    /**
     * @param $exception
     * @return mixed
     * @throws \Exception
     */
    private function handleErrorResponse($exception): mixed
    {
        $this->responseText = $exception->getResponse()->getBody()->getContents();

        if (helper::isValidJSON($this->responseText)) {
            return $this->responses->load(helper::decodeJSON($this->responseText));
        }

        throw $exception;
    }

    /**
     * @return string|null
     */
    function getResponseText(): ?string
    {
        return $this->responseText;
    }

    /**
     * @return string
     */
    private function getEndpoint(): string
    {
        if (count($this->requests) === 1) {
            return $this->requests[0]->endpoint;
        }
        return $this->batchEndpoint;
    }

    /**
     * @return $this
     */
    function reset(): client
    {
        $this->requests = array();
        return $this;
    }

    /**
     * @param $response
     * @return bool
     */
    private function isBatchResponse($response): bool
    {
        return isset($response->responses);
    }

}