<?php

namespace FoxentryPHP;

class helper
{

    /**
     * @param $input
     * @return bool
     */
    public static function isValidJSON($input): bool
    {
        if (is_array($input) or is_object($input)) {
            return false;
        }
        json_decode($input);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @param $data
     */
    public static function encodeJSON($data): bool|string
    {
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param $data
     */
    public static function decodeJSON($data): mixed
    {
        if (self::isValidJSON($data)) {
            return json_decode($data);
        }

        return null;
    }

}