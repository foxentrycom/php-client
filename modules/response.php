<?php

namespace FoxentryPHP;

use FoxentryPHP\response\error;
use FoxentryPHP\response\result;

class response
{

    private ?object $data;
    private ?result $result;
    private ?result $resultCorrected;
    private array $errors = array();
    private array $results = array();
    private array $suggestions = array();

    public function __construct(
        private error $errorObj,
        private result $resultObj
    )
    {}

    /**
     * @return void
     */
    public function __clone(): void
    {
        $this->reset();
    }

    /**
     * @return void
     */
    function reset(): void
    {
        $this->data = null;
        $this->errors = array();
        $this->result = null;
        $this->resultCorrected = null;
        $this->results     = array();
        $this->suggestions = array();
    }

    /**
     * @param object $data
     * @return $this
     */
    function load(object $data): response
    {
        $this->data = $data;
        $this->loadErrors();
        $this->loadResult();
        $this->loadResultCorrected();
        $this->loadSuggestions();
        $this->loadResults();

        return $this;
    }

    /**
     * @return void
     */
    function loadErrors(): void
    {
        if (isset($this->data->errors)) {
            foreach ($this->data->errors as $error) {
                $this->errors[] = (clone $this->errorObj)->load($error);
            }
        }
    }

    /**
     * @return void
     */
    function loadResults(): void
    {
        if (isset($this->data->response->results)) {
            foreach ($this->data->response->results as $result) {
                $this->results[] = (clone $this->resultObj)->load($result);
            }
        }
    }

    /**
     * @return void
     */
    private function loadResult(): void
    {
        $this->result = clone $this->resultObj;
        $this->result->load($this->data->response->result ?? null);
    }

    /**
     * @return void
     */
    private function loadResultCorrected(): void
    {
        $this->resultCorrected = clone $this->resultObj;
        $this->resultCorrected->load($this->data->response->resultCorrected ?? null);
    }

    /**
     * @return void
     */
    function loadSuggestions(): void
    {
        if (isset($this->data->response->suggestions)) {
            foreach ($this->data->response->suggestions as $suggestion) {
                $this->suggestions[] = (clone $this->resultObj)->load($suggestion);
            }
        }
    }

    /**
     * @return object|null
     */
    function get(): ?object
    {
        return $this->data;
    }

    /**
     * @return int
     */
    function getStatus(): int
    {
        return $this->data->status;
    }

    /**
     * @return array
     */
    function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return string|null
     */
    function getRequestEndpoint() : ?string
    {
        return $this->data->request->endpoint ?? null;
    }

    /**
     * @return string|null
     */
    function getRequestCode() : ?string
    {
        return $this->data->request->code ?? null;
    }

    /**
     * @return string|null
     */
    function getRequestCustomId() : ?string
    {
        return $this->data->request->customId ?? null;
    }

    /**
     * @return object|null
     */
    function getRequestQuery() : ?object
    {
        return $this->data->request->query ?? new \stdClass();
    }

    /**
     * @return object|null
     */
    function getRequestOptions() : ?object
    {
        return $this->data->request->options ?? new \stdClass();
    }

    /**
     * @return object|null
     */
    function getRequestClient() : ?object
    {
        return $this->data->request->client ?? new \stdClass();
    }

    /**
     * @return array
     */
    function getResults(): array
    {
        return $this->results;
    }

    /**
     * @return result|null
     */
    function getResult(): ?result
    {
        return $this->result;
    }

    /**
     * @return result|null
     */
    function getResultCorrected(): ?result
    {
        return $this->resultCorrected;
    }

    /**
     * @return array
     */
    function getSuggestions(): array
    {
        return $this->suggestions;
    }

}