<?php

namespace FoxentryPHP\endpoints\phone;

use FoxentryPHP\endpoints\phone;

class validate extends phone
{

    private string $endpoint = "phone/validate";

    public function __construct()
    {
        $this->setEndpoint($this->endpoint);
    }

    /**
     * @param string $validationType
     * @return $this
     */
    function setValidationType(string $validationType): static
    {
        $this->setOption("validationType", $validationType);
        return $this;
    }

}