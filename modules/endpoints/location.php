<?php

namespace FoxentryPHP\endpoints;

abstract class location extends endpoint
{

    /**
     * @param ?string $country
     * @return $this
     */
    function setCountry(?string $country): static
    {
        $this->setQueryParameter("country", $country);
        return $this;
    }

    /**
     * @param $street
     * @return $this
     */
    function setStreet(?string $street) {
        $this->setQueryParameter("street", $street);
        return $this;
    }

    /**
     * @param string|int|null $number
     * @return $this
     */
    function setNumber(string|int|null $number): static
    {
        $this->setQueryParameter("number", $number);
        return $this;
    }

    /**
     * @param ?string $streetWithNumber
     * @return $this
     */
    function setStreetWithNumber(?string $streetWithNumber): static
    {
        $this->setQueryParameter("streetWithNumber", $streetWithNumber);
        return $this;
    }

    /**
     * @param ?string $city
     * @return $this
     */
    function setCity(?string $city): static
    {
        $this->setQueryParameter("city", $city);
        return $this;
    }

    /**
     * @param string|int $zip
     * @return $this
     */
    function setZip(string|int $zip): static
    {
        $this->setQueryParameter("zip", $zip);
        return $this;
    }

    /**
     * @param ?string $full
     * @return $this
     */
    function setFull(?string $full): static
    {
        $this->setQueryParameter("full", $full);
        return $this;
    }
}