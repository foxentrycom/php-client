<?php

namespace FoxentryPHP\endpoints\email;

use FoxentryPHP\endpoints\email;
use FoxentryPHP\endpoints\endpoint;

class validate extends email
{

    private string $endpoint = "email/validate";

    public function __construct()
    {
        $this->setEndpoint($this->endpoint);
    }

    /**
     * @param string $validationType
     * @return $this
     */
    function setValidationType(string $validationType): static
    {
        $this->setOption("validationType", $validationType);
        return $this;
    }

}