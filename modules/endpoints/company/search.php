<?php

namespace FoxentryPHP\endpoints\company;

use FoxentryPHP\endpoints\endpoint;

class search extends endpoint
{

    private string $endpoint = "company/search";

    public function __construct()
    {
        $this->setEndpoint($this->endpoint);
    }

    /**
     * @param string $type
     * @return $this
     */
    function setType(string $type): static
    {
        $this->setQueryParameter("type", $type);
        return $this;
    }

    /**
     * @param string|int $value
     * @return $this
     */
    function setValue(string|int $value): static
    {
        $this->setQueryParameter("value", $value);
        return $this;
    }

    /**
     * @param string $parameter
     * @param string|int $value
     * @return $this
     */
    function setFilterParameter(string $parameter, string|int $value): static
    {
        $this->setQueryParameter("filter.".$parameter, $value);
        return $this;
    }

}