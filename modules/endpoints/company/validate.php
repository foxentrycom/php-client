<?php

namespace FoxentryPHP\endpoints\company;

use FoxentryPHP\endpoints\company;

class validate extends company
{

    private string $endpoint = "company/validate";

    public function __construct()
    {
        $this->setEndpoint($this->endpoint);
    }

}