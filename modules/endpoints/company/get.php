<?php

namespace FoxentryPHP\endpoints\company;

use FoxentryPHP\endpoints\company;

class get extends company
{

    private string $endpoint = "company/get";

    public function __construct()
    {
        $this->setEndpoint($this->endpoint);
    }

}