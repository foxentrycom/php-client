<?php

namespace FoxentryPHP\endpoints\name;

use FoxentryPHP\endpoints\name;

class validate extends name
{

    private string $endpoint = "name/validate";

    public function __construct()
    {
        $this->setEndpoint($this->endpoint);
    }

}