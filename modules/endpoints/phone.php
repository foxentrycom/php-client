<?php

namespace FoxentryPHP\endpoints;

abstract class phone extends endpoint
{

    /**
     * @param string|int|null $prefix
     * @return $this
     */
    function setPrefix(string|int|null $prefix): static
    {
        $this->setQueryParameter("prefix", $prefix);
        return $this;
    }

    /**
     * @param string|int|null $number
     * @return $this
     */
    function setNumber(string|int|null $number): static
    {
        $this->setQueryParameter("number", $number);
        return $this;
    }
}