<?php

namespace FoxentryPHP\endpoints;

use FoxentryPHP\request;

abstract class endpoint extends request
{

    /**
     * @param string $dataScope
     * @return $this
     */
    function setDataScope(string $dataScope): static
    {
        $this->setOption("dataScope", $dataScope);
        return $this;
    }

    /**
     * @param int $resultsCount
     * @return $this
     */
    function setResultsLimit(int $resultsCount): static
    {
        $this->setOption("resultsLimit", $resultsCount);
        return $this;
    }

}