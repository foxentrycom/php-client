<?php

namespace FoxentryPHP\endpoints;

abstract class email extends endpoint
{

    /**
     * @param $email
     * @return $this
     */
    function setEmail(string $email): static
    {
        $this->setQueryParameter("email", $email);
        return $this;
    }

}