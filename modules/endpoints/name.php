<?php

namespace FoxentryPHP\endpoints;

abstract class name extends endpoint
{

    /**
     * @param ?string $name
     * @return $this
     */
    function setName(?string $name): static
    {
        $this->setQueryParameter("name", $name);
        return $this;
    }

    /**
     * @param ?string $surname
     * @return $this
     */
    function setSurname(?string $surname): static
    {
        $this->setQueryParameter("surname", $surname);
        return $this;
    }

    /**
     * @param ?string $nameSurname
     * @return $this
     */
    function setNameSurname(?string $nameSurname): static
    {
        $this->setQueryParameter("nameSurname", $nameSurname);
        return $this;
    }

}