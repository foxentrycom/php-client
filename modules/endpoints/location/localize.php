<?php

namespace FoxentryPHP\endpoints\location;

use FoxentryPHP\endpoints\location;

class localize extends location
{

    private string $endpoint = "location/localize";

    public function __construct()
    {
        $this->setEndpoint($this->endpoint);
    }

    /**
     * @param float $latitude
     * @return $this
     */
    function setLatitude(float $latitude): static
    {
        $this->setQueryParameter("lat", $latitude);
        return $this;
    }

    /**
     * @param float $longitude
     * @return $this
     */
    function setLongitude(float $longitude): static
    {
        $this->setQueryParameter("lon", $longitude);
        return $this;
    }

}