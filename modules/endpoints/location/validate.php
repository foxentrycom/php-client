<?php

namespace FoxentryPHP\endpoints\location;

use FoxentryPHP\endpoints\location;

class validate extends location
{

    private string $endpoint = "location/validate";

    public function __construct()
    {
        $this->setEndpoint($this->endpoint);
    }

}