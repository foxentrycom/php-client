<?php

namespace FoxentryPHP\endpoints\location;

use FoxentryPHP\endpoints\location;

class get extends location
{

    private string $endpoint = "location/get";

    public function __construct()
    {
        $this->setEndpoint($this->endpoint);
    }

    /**
     * @param string|int $addressId
     * @return $this
     */
    function setId(string|int $addressId): static
    {
        $this->setQueryParameter("id", $addressId);
        return $this;
    }

    /**
     * @param string $idType
     * @return $this
     */
    function setIdType(string $idType): static
    {
        $this->setOption("idType", $idType);
        return $this;
    }

    /**
     * @param string|int $internalAddressId
     * @return $this
     */
    function setInternalId(string|int $internalAddressId): static
    {
        $this->setId($internalAddressId);
        $this->setIdType("internal");
        return $this;
    }

    /**
     * @param string|int $externalAddressId
     * @return $this
     */
    function setExternalId(string|int $externalAddressId): static
    {
        $this->setId($externalAddressId);
        $this->setIdType("external");
        return $this;
    }

}