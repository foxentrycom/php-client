<?php

namespace FoxentryPHP\endpoints\location;

use FoxentryPHP\endpoints\location;

class search extends location
{

    private string $endpoint = "location/search";

    public function __construct()
    {
        $this->setEndpoint($this->endpoint);
    }

    /**
     * @param string $type
     * @return $this
     */
    function setType(string $type): static
    {
        $this->setQueryParameter("type", $type);
        return $this;
    }

    /**
     * @param string|int $value
     * @return $this
     */
    function setValue(string|int $value): static
    {
        $this->setQueryParameter("value", $value);
        return $this;
    }

    /**
     * @param string $parameter
     * @param string|int $value
     * @return $this
     */
    function setFilterParameter(string $parameter, string|int $value): static
    {
        $this->setQueryFilterParameter($parameter, $value);
        return $this;
    }

}