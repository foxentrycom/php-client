<?php

namespace FoxentryPHP\endpoints;

abstract class company extends endpoint
{

    /**
     * @param ?string $country
     * @return $this
     */
    function setCountry(?string $country): static
    {
        $this->setQueryParameter("country", $country);
        return $this;
    }

    /**
     * Set company name
     *
     * @param ?string $name
     * @return $this
     */
    function setName(?string $name): static
    {
        $this->setQueryParameter("name", $name);
        return $this;
    }


    /**
     * @param string|int|null $registrationNumber
     * @return $this
     */
    function setRegistrationNumber(string|int|null $registrationNumber): static
    {
        $this->setQueryParameter("registrationNumber", $registrationNumber);
        return $this;
    }

    /**
     * @param string|int|null $taxNumber
     * @return $this
     */
    function setTaxNumber(string|int|null $taxNumber): static
    {
        $this->setQueryParameter("taxNumber", $taxNumber);
        return $this;
    }

    /**
     * @param ?string $vatNumber
     * @return $this
     */
    function setVatNumber(?string $vatNumber): static
    {
        $this->setQueryParameter("vatNumber", $vatNumber);
        return $this;
    }


}