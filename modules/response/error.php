<?php

namespace FoxentryPHP\response;

class error
{

    private ?string $group;
    private ?string $type;
    private ?string $subtype;
    private ?string $severity;
    private ?array $relatedTo;
    private ?string $description;

    /**
     * @return void
     */
    function reset(): void
    {
        $this->group = null;
        $this->type = null;
        $this->subtype = null;
        $this->severity = null;
        $this->relatedTo = array();
        $this->description = null;
    }

    /**
     * @param object $error
     * @return $this
     */
    function load(object $error): static
    {
        $this->group       = $error->group ?? null;
        $this->type        = $error->type ?? null;
        $this->subtype     = $error->subtype ?? null;
        $this->severity    = $error->severity ?? null;
        $this->relatedTo   = $error->relatedTo ?? array();
        $this->description = $error->description ?? null;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGroup(): ?string
    {
        return $this->group;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getSubtype(): ?string
    {
        return $this->subtype;
    }

    /**
     * @return string|null
     */
    public function getSeverity(): ?string
    {
        return $this->severity;
    }

    /**
     * @return array|null
     */
    public function getRelatedTo(): ?array
    {
        return $this->relatedTo;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

}