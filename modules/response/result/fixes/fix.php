<?php

namespace FoxentryPHP\response\result\fixes;

class fix
{

    private ?string $group;
    private ?string $type;
    private ?string $subtype;
    private ?string $typeFrom;
    private ?string $typeTo;
    private ?string $valueFrom;
    private ?string $valueTo;

    /**
     * @return void
     */
    function reset(): void
    {
        $this->group = null;
        $this->type = null;
        $this->subtype = null;
        $this->typeFrom = null;
        $this->typeTo= null;
        $this->valueFrom = null;
        $this->valueTo = null;
    }

    /**
     * @param object $fix
     * @return $this
     */
    function load(object $fix): static
    {
        $this->group      = $fix->group ?? null;
        $this->type       = $fix->type ?? null;
        $this->subtype    = $fix->subtype ?? null;
        $this->typeFrom   = $fix->typeFrom ?? null;
        $this->typeTo     = $fix->type ?? null;
        $this->valueFrom  = $fix->valueFrom ?? null;
        $this->valueTo    = $fix->valueTo ?? null;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGroup(): ?string
    {
        return $this->group;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getSubtype(): ?string
    {
        return $this->subtype;
    }

    /**
     * @return string|null
     */
    public function getTypeFrom(): ?string
    {
        return $this->typeFrom;
    }

    /**
     * @return string|null
     */
    public function getTypeTo(): ?string
    {
        return $this->typeTo;
    }

    /**
     * @return string|null
     */
    public function getValueFrom(): ?string
    {
        return $this->valueFrom;
    }

    /**
     * @return string|null
     */
    public function getValueTo(): ?string
    {
        return $this->valueTo;
    }

}