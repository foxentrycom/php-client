<?php

namespace FoxentryPHP\response\result;

use FoxentryPHP\response\result\fixes\fix;

class fixes
{

    private array $list = array();

    public function __construct(
        private fix $fix
    )
    {}

    /**
     * @return void
     */
    function reset(): void
    {
        $this->list = array();
    }

    /**
     * @param array $fixes
     * @return $this
     */
    function load(array $fixes): static
    {
        $this->reset();

        foreach ($fixes as $fix) {
            $this->list[] = (clone $this->fix)->load($fix);
        }

        return $this;
    }

    /**
     * @return array
     */
    function getList(): array
    {
        return $this->list;
    }

}