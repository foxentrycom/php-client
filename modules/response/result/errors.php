<?php

namespace FoxentryPHP\response\result;

use FoxentryPHP\response\result\errors\error;

class errors
{

    private array $list = array();

    public function __construct(
        private error $error
    )
    {}

    /**
     * @return void
     */
    function reset(): void
    {
        $this->list = array();
    }

    /**
     * @param array $errors
     * @return $this
     */
    function load(array $errors): static
    {
        $this->reset();

        foreach ($errors as $error) {
            $this->list[] = (clone $this->error)->load($error);
        }

        return $this;
    }

    /**
     * @return array
     */
    function getList(): array
    {
        return $this->list;
    }

}