<?php

namespace FoxentryPHP\response;

use FoxentryPHP\response;

class result
{

    private object $result;
    private response\result\errors $errors;
    private response\result\fixes $fixes;

    public function __construct(
        private response\result\errors $errorsObj,
        private response\result\fixes $fixesObj
    )
    {}

    /**
     * @return void
     */
    function __clone() {
        $this->reset();
    }

    /**
     * @return void
     */
    private function reset(): void
    {
        $this->result = new \stdClass();
        $this->errors = (clone $this->errorsObj);
        $this->fixes  = (clone $this->fixesObj);
    }

    /**
     * @param ?object $result
     * @return ?result
     */
    public function load(?object $result): ?result
    {
        if (is_null($result)) {
            return null;
        }
        else {
            $this->reset();
            $this->result = $result;
            $this->errors->load($result->errors ?? array());
            $this->fixes->load($result->fixes ?? array());

            return $this;
        }
    }

    /**
     * @return ?bool
     */
    function isValid() :?bool
    {
        return $this->result->isValid ?? null;
    }

    /**
     * @return string|null
     */
    function getProposal() :?string
    {
        return $this->result->proposal ?? null;
    }

    /**
     * @param string|null $path
     * @return mixed
     */
    function getData(?string $path = null) :mixed
    {
        if (is_null($path)) {
            return $this->result->data;
        }

        $ex = explode(".", $path);
        $helper = $this->result->data;

        foreach ($ex as $parameter) {
            if (!isset($helper->$parameter)) {
                return null;
            }

            $helper = $helper->$parameter;
        }

        return $helper;
    }

    /**
     * @return array
     */
    function getErrors(): array
    {
        return $this->errors->getList();
    }

    /**
     * @return array
     */
    function getFixes(): array
    {
        return $this->fixes->getList();
    }

    /**
     * @return bool
     */
    function hasErrors(): bool
    {
        return count($this->getErrors()) > 0;
    }

    /**
     * @return bool
     */
    function hasFixes(): bool
    {
        return count($this->getFixes()) > 0;
    }

}