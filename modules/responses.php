<?php

namespace FoxentryPHP;

use FoxentryPHP\response\error;

class responses
{

    private object $data;
    private array $errors = array();
    private array $list = array();
    private int $status;

    public function __construct(
        private response $response,
        private error $errorObj
    )
    {}

    /**
     * @return void
     */
    private function reset(): void
    {
        $this->data = new \stdClass();
        $this->list = array();
        $this->errors = array();
    }

    /**
     * @param object $data
     * @return $this
     */
    function load(object $data): static
    {
        $this->reset();

        $this->data = $data;
        $this->setStatus($data->status);
        $this->loadResponses();
        $this->loadErrors();

        return $this;
    }

    /**
     * @param object $data
     * @return $this
     */
    function loadSingle(object $data): static
    {
        $this->reset();

        $this->data = $data;
        $this->setStatus($data->status);
        $this->loadResponse($data);
        $this->loadErrors();

        return $this;
    }

    /**
     * @return void
     */
    private function loadResponses(): void
    {
        if (isset($this->data->responses)) {
            foreach ($this->data->responses as $response) {
                $this->loadResponse($response);
            }
        }
    }

    /**
     * @param $response
     * @return void
     */
    private function loadResponse($response): void
    {
        $this->list[] = (clone $this->response)->load($response);
    }

    /**
     * @return void
     */
    function loadErrors(): void
    {
        if (isset($this->data->errors)) {
            foreach ($this->data->errors as $error) {
                $this->errors[] = (clone $this->errorObj)->load($error);
            }
        }
    }

    /**
     * @return bool
     */
    function successful(): bool
    {
        return ($this->getStatus() === 200 OR $this->getStatus() === 207);
    }

    /**
     * @param int $status
     * @return void
     */
    private function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return error[]
     */
    function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return object
     */
    function getData(): object
    {
        return $this->data;
    }

    /**
     * @return response[]
     */
    function getResponses(): array
    {
        return $this->list;
    }

    /**
     * @return ?response
     */
    function getResponse(): ?response
    {
        return $this->list[0] ?? null;
    }

    /**
     * @param string|int $requestCustomId
     * @return response|null
     */
    function getResponseByRequestCustomId(string|int $requestCustomId): ?response
    {
        foreach ($this->getResponses() as $response) {
            if ($response->getRequestCustomId() === $requestCustomId) {
                return $response;
            }
        }
        return null;
    }


}