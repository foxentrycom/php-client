# Foxentry API PHP Client


## **This library is no longer usable. Please use our newest [PHP library](https://github.com/Foxentry/PHP-LIB).**


===================================================


This is PHP library for working with API services provided by [Foxentry.com](https://foxentry.com). and documented at [apiary](https://foxentryapiv2.docs.apiary.io).

You can find out how this library works by checking examples located in examples folder. 
This examples contains everything you need for implementing our API to your application,
but if you are having troubles with implementation or have some questions, do not hesitate to contact us by sending email to info@foxentry.com

## Installation

Library is available for installation by composer. Just run this command in your project folder or require *foxentry/phpclient* in your composer.json file.

> composer require foxentry/phpclient

## API key

For using our API you have to get API key, so just register at [Foxentry APP](https://app.foxentry.com) and create new API project.

