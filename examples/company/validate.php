<?php

use FoxentryPHP\client;

include_once realpath(dirname(__FILE__))."/../../vendor/autoload.php";

$client = new client();
$client->setApiKey("your_api_key");
$client->setApiVersion("2.0");

$apiCall = $client
    ->addRequest(
        (new \FoxentryPHP\endpoints\company\validate)
            ->setCountry("CZ")
            ->setName("Avantro s.r.o.")
            ->setDataScope("basic")
    )
    ->send();

if ($apiCall->successful()) {
    print_r($apiCall->getResponse()->get());
}
else {
    echo $apiCall->getErrors()[0]->getDescription();
}