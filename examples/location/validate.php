<?php

use FoxentryPHP\client;

include_once realpath(dirname(__FILE__))."/../../vendor/autoload.php";

$client = new client();
$client->setApiKey("your_api_key");
$client->setApiVersion("2.0");

$apiCall = $client
    ->addRequest(
        (new \FoxentryPHP\endpoints\location\validate)
            ->setStreetWithNumber("Thámova 137/16")
            ->setCity("Praha")
            ->setZip("18600")
            ->setDataScope("basic")
    )
    ->send();

if ($apiCall->successful()) {
    print_r($apiCall->getResponse()->get());
}
else {
    echo $apiCall->getErrors()[0]->getDescription();
}