<?php

use FoxentryPHP\client;

include_once realpath(dirname(__FILE__))."/../vendor/autoload.php";

$client = new client();
$client->setApiKey("your_api_key");
$client->setApiVersion("2.0");

$apiCall = $client
    ->addRequest(
        (new \FoxentryPHP\endpoints\location\validate)
            ->setStreetWithNumber("Thámova 137/16")
            ->setCity("Praha")
            ->setZip("186 00")
            ->setOptions(array(
                "cityFormat" => "minimal",
                "zipFormat" => "standard"
            ))
    )
    ->addRequest(
        (new \FoxentryPHP\endpoints\location\validate)
            ->setStreetWithNumber("Jeseniova 1151/55")
            ->setCity("Praha")
            ->setZip("130 00")
    )
    ->addRequest(
        (new \FoxentryPHP\endpoints\location\validate)
            ->setStreetWithNumber("Hrad I. nádvoří 1")
            ->setCity("Praha")
    )
    ->send();


if ($apiCall->successful()) {
    foreach ($apiCall->getResponses() as $singleResponse) {
        print_r($singleResponse->get());
    };
}
else {
    echo $apiCall->getErrors()[0]->getDescription();
}
